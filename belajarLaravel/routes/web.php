<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'home']
);
Route::get('/register', [AuthController::class,'register']
);
Route::post('/welcome', [AuthController::class,'kirim']
);
Route::get('/table', function(){
    return view('table.table');
}
);
Route::get('/data-table', function(){
    return view('table.data-table');
}
);

Route::group([
    'prefix'=>'post',
    'name'=>'post.'
],function(){
Route::get('/', [PostController::class,'index'])->name('index');
Route::get('/create', [PostController::class,'create'])->name('create');
Route::post('/', [PostController::class,'store'])->name('store');
Route::get('/{post}', [PostController::class,'show'])->name('show');
Route::get('/{post}/edit', [PostController::class,'edit'])->name('edit');
Route::put('/{post}', [PostController::class,'update'])->name('update');
Route::delete('/{post}', [PostController::class,'destroy'])->name('destroy');
});
Route::group([
    'prefix'=>'cast',
    'as'=>'cast.'
],function(){
Route::get('/', [CastController::class,'index'])->name('index');
Route::get('/create', [CastController::class,'create'])->name('create');
Route::post('/', [CastController::class,'store'])->name('store');
Route::get('/{cast}', [CastController::class,'show'])->name('show');
Route::get('/{cast}/edit', [CastController::class,'edit'])->name('edit');
Route::put('/{cast}', [CastController::class,'update'])->name('update');
Route::delete('/{cast}', [CastController::class,'destroy'])->name('destroy');
});