<DOCTYPE html>
  <html>
    <body>
      <h1>Buat Account Baru!</h1>
      <h2>Sign Up Form</h2>

      <form action="/welcome" method="POST">
      @csrf 
      <label> First Name: </label><br /><br />
        <input type="text" name="firstname" /><br /><br />
        <label> Last Name: </label><br /><br />
        <input type="text" name="lastname" /><br /><br />
        <label> Gender: </label><br /><br />
        <input type="radio" name="gender" />Male<br />
        <input type="radio" name="gender" />Female<br />
        <input type="radio" name="gender" />Other<br /><br />
        <label>Nationality</label><br /><br />
        <select name="national">
          <option value="indo">Indonesia</option>
          <option value="amrik">Amerika</option>
          <option value="english">Inggris</option>
        </select>
        <br /><br />
        <label> Language Spoken </label><br /><br />
        <input type="checkbox" name="indo" />Bahasa Indonesia<br />
        <input type="checkbox" name="english" />English<br />
        <input type="checkbox" name="others" />Others<br /><br />
        <label>Bio</label><br /><br />
        <textarea name="message" rows="5" cols="100"></textarea>
        <br /><br />
        <input type="submit" value="Sign Up" />
      </form>
    </body>
  </html>
</DOCTYPE>
