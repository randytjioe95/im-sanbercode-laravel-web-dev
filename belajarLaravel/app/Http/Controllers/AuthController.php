<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $first=$request['firstname'];
         $last=$request['lastname'];
         return view('halaman.welcome',compact('first','last'));
    }

}
